#!/bin/bash

cd mysql
./run.sh "mysql"
cd ..
sleep 20

cd talkplanning
./run.sh
cd ..

cd nginx-proxy
./run.sh
cd ..
