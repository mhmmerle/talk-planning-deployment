#!/bin/bash

docker stop talkplanning
docker rm talkplanning
docker pull mhmmerle/talk-planning:1.0-SNAPSHOT
docker build -t talkplanning .

docker run --name talkplanning --link mysql:database \
-e TALKPLANNING_CORS_ALLOWEDORIGIN=https://$TP_DOMAIN_NAME \
-e SPRING_DATASOURCE_PASSWORD=$TP_DB_PASSWORD \
-e TP_MAIL_SMTP_PASSWORD=$TP_MAIL_SMTP_PASSWORD \
-e TP_MAIL_SMTP_HOST=$TP_MAIL_SMTP_HOST \
-e TP_MAIL_SMTP_USER=$TP_MAIL_SMTP_USER \
-d talkplanning