#!/bin/bash

INPUT_FILE=$1

mkdir -p restore
tar --force-local -C restore -xvf $INPUT_FILE

cat restore/dump.sql | docker exec -i mysql mysql -u talkplanning -p"$TP_DB_PASSWORD"

docker cp restore/letsencrypt talkplanning-nginx-proxy:/etc/
docker exec -i -t talkplanning-nginx-proxy sed -i -e s/#ssl//g /etc/nginx/conf.d/nginx-proxy.conf
docker exec -i -t talkplanning-nginx-proxy service nginx reload

rm -rf restore