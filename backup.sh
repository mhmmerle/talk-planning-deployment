#!/bin/bash

DATE=`date '+%Y-%m-%d-%H:%M:%S'`

mkdir -p backup
docker exec mysql sh -c "exec mysqldump --databases talkplanning -u talkplanning -p\"$TP_DB_PASSWORD\"" > backup/dump.sql
docker cp talkplanning-nginx-proxy:/etc/letsencrypt backup/
tar --force-local -C backup -cvf "backup-${DATE}.tar" dump.sql letsencrypt
rm -rf backup