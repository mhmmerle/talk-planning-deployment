#!/usr/bin/env bash

IMAGE_NAME="talkplanning-nginx-proxy"

docker exec -i -t $IMAGE_NAME certbot certonly --staging --webroot -w /var/www/letsencrypt/ -d $TP_DOMAIN_NAME; -m $TP_ADMIN_EMAIL --agree-tos
docker exec -i -t $IMAGE_NAME sed -i -e s/#ssl//g  /etc/nginx/conf.d/nginx-proxy.conf
docker exec -i -t $IMAGE_NAME service nginx reload