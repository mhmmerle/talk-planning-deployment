#!/bin/bash
IMAGE_NAME="talkplanning-nginx-proxy"
envsubst '${TP_DOMAIN_NAME}' < conf/nginx-proxy.conf.template > nginx-proxy.conf
docker stop $IMAGE_NAME
docker rm $IMAGE_NAME
docker build -t $IMAGE_NAME .
docker run --name $IMAGE_NAME --link talkplanning -p 80:80 -p 443:443 -d -e ADMIN_EMAIL=${TP_ADMIN_EMAIL} -e DOMAIN=${TP_DOMAIN_NAME} $IMAGE_NAME
#docker exec $IMAGE_NAME certbot certonly --staging --webroot -w /var/www/talkplanning-local.com/ -d talkplanning-local.com -d test.talkplanning-local.com
#docker exec $IMAGE_NAME certbot-auto renew --quiet --no-self-upgrade --renew-hook "service nginx restart"