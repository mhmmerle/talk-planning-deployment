#!/usr/bin/env bash

MYSQL_ROOT_PASSWORD=$(docker logs mysql | sed 's/^.*GENERATED ROOT PASSWORD: //;tx;d;:x')
echo "$MYSQL_ROOT_PASSWORD"