#!/usr/bin/env bash

IMAGE_NAME="mysql/mysql-server:latest"

docker stop mysql
docker rm mysql

docker run --name mysql -e MYSQL_RANDOM_ROOT_PASSWORD=yes -e MYSQL_USER=talkplanning -e MYSQL_PASSWORD=$TP_DB_PASSWORD -e MYSQL_DATABASE=talkplanning -d $IMAGE_NAME