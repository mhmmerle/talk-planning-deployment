# talk-planning-deployment

Environment variables that must be set to use these deployment scripts:
TP_ADMIN_EMAIL = Email of the admin used for domain registration
TP_DOMAIN_NAME = Domain of the talkplanning system used for getting the certificate
TP_DB_PASSWORD = Password that is used to connect to the database