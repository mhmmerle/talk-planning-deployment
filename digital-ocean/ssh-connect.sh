#!/usr/bin/env bash

# IP-Adresse des Droplet abfragen
IP_ADDRESS=$(doctl compute  droplet list | grep talkplanning-test | sed -e 's/^.* \([0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+\).*/\1/')

ssh root@$IP_ADDRESS