#!/usr/bin/env bash

# Public-Key des Computers im Digitalocean-Account eintragen
doctl compute ssh-key import goliath --public-key-file ~/.ssh/id_rsa.pub

# Id des SSH-Key ermitteln
SSH_KEY_ID_LOCAL=$(doctl compute ssh-key list | grep goliath | sed -e 's/^\([0-9]\+\).*/\1/')

# Droplet erstellen
doctl compute droplet create talkplanning-test --image docker-16-04 --region fra1 --size s-1vcpu-3gb --ssh-keys $SSH_KEY_ID_LOCAL --wait

# IP-Adresse des Droplet abfragen
IP_ADDRESS=$(doctl compute  droplet list | grep talkplanning-test | sed -e 's/^.* \([0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+\).*/\1/')

# SSH-Konfiguration anpassen, so dass für SSH-Verbindung der Host-Key des Droplet nicht geprüft wird
echo "Host $IP_ADDRESS
StrictHostKeyChecking no
UserKnownHostsFile=/dev/null" > ~/.ssh/config
chmod 600 ~/.ssh/config

echo "Continue with preparing the droplet with ./prepare-droplet.sh"
