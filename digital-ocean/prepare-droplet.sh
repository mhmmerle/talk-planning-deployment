#!/usr/bin/env bash

function askFor {
   while [ -z "${!1}" ]; do
      echo "$2"
      read "$1"
   done
}

askFor TP_DOMAIN_NAME "Domain-Name:"
askFor TP_ADMIN_EMAIL "E-Mail-Adresse des Domain-Admin:"
askFor TP_DOCKER_AUTHTOKEN "Authorization Token für Docker Hub Account:"
askFor TP_MAIL_SMTP_HOST "SMTP Mail Service host"
askFor TP_MAIL_SMTP_USER "SMTP Mail Service username"
askFor TP_MAIL_SMTP_PASSWORD "SMTP Mail Service password"

TP_DB_PASSWORD=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-32};echo;)

# IP-Adresse des Droplet abfragen
IP_ADDRESS=$(doctl compute  droplet list | grep talkplanning-test | sed -e 's/^.* \([0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+\).*/\1/')

# Umgebungs-Variablen für Betrieb der Container eintragen
ssh root@$IP_ADDRESS "echo '
export TP_ADMIN_EMAIL=$TP_ADMIN_EMAIL;
export TP_DOMAIN_NAME=$TP_DOMAIN_NAME;
export TP_MAIL_SMTP_HOST=$TP_MAIL_SMTP_HOST;
export TP_MAIL_SMTP_USER=$TP_MAIL_SMTP_USER;
export TP_MAIL_SMTP_PASSWORD=$TP_MAIL_SMTP_PASSWORD;
export TP_DB_PASSWORD=$TP_DB_PASSWORD' >> ~/.bashrc"

sleep 3
# Authorization Token hinterlegen, für Zugriff auf Talkplanning-Docker-Image im Docker Hub
ssh root@$IP_ADDRESS "mkdir ~/.docker; echo '{  \"auths\": {
    \"https://index.docker.io/v1/\": {
      \"auth\": \"$TP_DOCKER_AUTHTOKEN\"
    }
  }
}
' > ~/.docker/config.json"


sleep 3
# Host-Key von Bitbucket auf dem Droplet in die known hosts eintragen, sodass beim Zugriff keine Abfragen mehr kommen
ssh root@$IP_ADDRESS "echo '|1|cTQZL4AKu3ybg2LE30K5jYDY0gU=|0hgYzPx1fRaYfsAzPOgmtuYnEsg= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==
|1|FBQW6AHna8jXcQVIcN3DT/H2Hjw=|RJtnQIRcEIXWKA10oust+2vWxjs= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==' >> ~/.ssh/known_hosts"

# SSH-Keys für lesenden Zugriff auf das Talkplanning-Deployment-Repo zum Droplet kopieren
# und Deployment-Repo klonen
scp id_rsa id_rsa.pub root@$IP_ADDRESS:~/.ssh/
ssh root@$IP_ADDRESS "chmod 600 .ssh/id_rsa*; git clone git@bitbucket.org:mhmmerle/talk-planning-deployment.git"

echo "Connect to your new droplet with \"ssh root@$IP_ADDRESS\""
echo "Run deployment scripts with \"cd talk-planning-deployment; ./runall.sh\""