#!/usr/bin/env bash

INPUT_FILEPATH=$1
INPUT_FILE=$(basename $INPUT_FILEPATH)

# IP-Adresse des Droplet abfragen
IP_ADDRESS=$(doctl compute  droplet list | grep talkplanning-test | sed -e 's/^.* \([0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+\).*/\1/')

scp "$INPUT_FILEPATH" root@$IP_ADDRESS:~/talk-planning-deployment/
ssh root@$IP_ADDRESS "cd talk-planning-deployment; ./restore.sh ${INPUT_FILE}"